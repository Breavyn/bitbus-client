# Installation

```
npm install --save git+https://gitlab.com/Breavyn/bitbus-client.git
```

# Example

Basic Usage

```javascript
const { Bitbus, Bitsocket } = require("bitbus-client");

const token = "<your planaria token>";
const bitbus = new Bitbus({ token, endpoint: Bitbus.endpoint.txo });
const bitsocket = new Bitsocket({ token, endpoint: Bitsocket.endpoint.bob });

bitbus.query({ v: 3, q: { find: {}, limit: 100 } }).then((resultStream) => {
  resultStream.on("data", (data) => {
    console.log(data);
  });
});
```

Local JQ processing of `r.f`. This functionality requires the `jq` executable
be available on your path.

```javascript
bitbus
  .query({ v: 3, q: { find: {}, limit: 100 }, r: { f: "{txid: .tx.h}" } })
  .then((resultStream) => {
    resultStream.on("data", (data) => {
      console.log(data);
    });
  });
```

Return an array instead of a stream

```javascript
bitbus
  .query({ v: 3, q: { find: {}, limit: 100 } }, { array: true })
  .then((resultArray) => {
    console.log(resultArray);
  });
```

Querying Bitsocket works in the same way as bitbus

```javascript
bitsocket
  .query(
    { v: 3, q: { find: {}, limit: 10 }, r: { f: "{txid: .tx.h}" } },
    { array: true }
  )
  .then((resultArray) => {
    console.log(resultArray);
  });
```

Listen to the mempool

```javascript
// lastEventId can optionally be provided to the listen function
const lastEventId = "5fcc19d2fa7b8764fc62a45d";
const eventSource = bitsocket.listen({ v: 3, q: { find: {} } }, lastEventId);
eventSource.on("data", (data) => {
  console.log("eventId", data._id);
  console.log(data);
});

// to stop listening, call eventSource.close();
```

Query bitbus status

```javascript
bitbus.status().then((status) => {
  console.log(status);
});
```

# References

- [bitquery](https://bitquery.planaria.network)
- [bitbus](https://docs.bitbus.network)
- [bitsocket](https://bitsocket.network)
- [planaria token](https://token.planaria.network/)
