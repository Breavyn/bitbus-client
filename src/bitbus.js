const axios = require("axios");

const responseTranformer = require("./utils/responsetransformer");

class Bitbus {
  constructor({ token, endpoint }) {
    this.endpoint = endpoint;
    this._queryPath = "/block";
    this._http = axios.create({
      baseURL: this.endpoint,
      headers: { token },
    });
  }

  async _query(query) {
    const { data } = await this._http.post(this._queryPath, query, {
      transformResponse: responseTranformer(query.r && query.r.f),
      headers: { Accept: "application/x-ndjson" },
      responseType: "stream",
    });

    return data;
  }

  query(query, { array = false } = {}) {
    if (array)
      return new Promise((resolve) => {
        this._query(query).then((responseStream) => {
          const responseData = [];
          responseStream.on("data", (data) => responseData.push(data));
          responseStream.on("end", () => resolve(responseData));
        });
      });

    return this._query(query);
  }

  async status() {
    const { data } = await this._http.get("/status");
    return data;
  }
}

Bitbus.endpoint = {
  txo: "https://txo.bitbus.network",
  bob: "https://bob.bitbus.network",
};

module.exports = Bitbus;
