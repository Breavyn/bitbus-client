const child_process = require("child_process");

const jq = (source, command) => {
  const jq = child_process.spawn("jq", ["--unbuffered", "-cM", command]);
  source.pipe(jq.stdin);
  return jq.stdout;
};

module.exports = jq;
