const split = require("split");

const jq = require("./jq");

const responseTranformer = (command) => {
  return (data) => {
    const source = command ? jq(data, command) : data;
    return source.pipe(split(JSON.parse, null, { trailing: false }));
  };
};

module.exports = responseTranformer;
