const EventSource = require("eventsource");

const Bitbus = require("./bitbus");

class Bitsocket extends Bitbus {
  constructor(opts) {
    super(opts);
    this._queryPath = "/crawl";
  }

  listen(query, lastEventId) {
    const queryString = Buffer.from(JSON.stringify(query)).toString("base64");

    const es = new EventSource(`${this.endpoint}/s/${queryString}`, {
      headers: { "Last-Event-ID": lastEventId },
    });

    es.on("message", (message) => {
      try {
        const data = JSON.parse(message.data);
        for (const record of data.data) {
          es.emit("data", record);
        }
      } finally {
        // eslint-ignore: no-empty
      }
    });

    return es;
  }

  status() {
    throw new Error("not implemented");
  }
}

Bitsocket.endpoint = {
  txo: "https://txo.bitsocket.network",
  bob: "https://bob.bitsocket.network",
};

module.exports = Bitsocket;
